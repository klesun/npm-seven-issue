See https://github.com/npm/cli/issues/1991#issuecomment-853812934

To reproduce the `npm` `v7.x` regression of `npm install` hanging for 60+ seconds follow these steps:

```bash
git clone https://gitlab.com/klesun/npm-seven-issue.git
cd npm-seven-issue
npm install
```

If you are using `npm` version prior to `v7.x`, for example `v6.14.13`, `npm install` should be done within 2-3 seconds.

If you are using `npm` version `v7.0.0` or later, for example `7.15.1`, `npm install` will hang for 60+ seconds with following text:
```
[....................] \ idealTree.pql-box: still idealTree buildDeps
```

Note, you need to remove `package-lock.json` and `node_modules` prior to retrying the `npm install`, otherwise issue won't reproduce.
